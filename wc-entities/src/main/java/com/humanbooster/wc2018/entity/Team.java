package com.humanbooster.wc2018.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Entity
@Table(name = "TEAMS")
public class Team implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "FIFA_CODE")
    private String fifaCode;

    @Column(name = "ISO2")
    private String iso2;

    @Column(name = "FLAG")
    private String flag;

    @Column(name = "EMOJI")
    private String emoji;

    @OneToMany(mappedBy = "homeTeam")
    private List<Match> homeMatchs;

    @OneToMany(mappedBy = "awayTeam")
    private List<Match> awayMatchs;

    @OneToMany(mappedBy = "finalWinner")
    private List<FinalMatch> matches;

    public Team() {

    }

    public Team(Long id, String name, String fifaCode, String iso2, String flag, String emoji) {
        this.id = id;
        this.name = name;
        this.fifaCode = fifaCode;
        this.iso2 = iso2;
        this.flag = flag;
        this.emoji = emoji;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFifaCode() {
        return fifaCode;
    }

    public void setFifaCode(String fifaCode) {
        this.fifaCode = fifaCode;
    }

    public String getIso2() {
        return iso2;
    }

    public void setIso2(String iso2) {
        this.iso2 = iso2;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getEmoji() {
        return emoji;
    }

    public void setEmoji(String emoji) {
        this.emoji = emoji;
    }
}
