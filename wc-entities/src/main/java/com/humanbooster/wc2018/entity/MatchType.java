package com.humanbooster.wc2018.entity;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public enum MatchType {
    GROUP,
    WINNER,
    LOSER,
    QUALIFIED
}
