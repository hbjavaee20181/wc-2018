package com.humanbooster.wc2018.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Entity
@Table(name = "GROUP_MATCHS")
public class GroupMatch extends Match {

    @ManyToOne
    @JoinColumn(name = "GROUP_ID")
    private Group group;

    public GroupMatch() {

    }

    public GroupMatch(Long name, MatchType type, Team homeTeam, Team awayTeam, Long homeResult, Long awayResult,
                      Date date, Stadium stadium, List<TvChannel> channels, Boolean finished, Integer matchDay, Group group) {
        super(name, type, homeTeam, awayTeam, homeResult, awayResult, date, stadium, channels, finished, matchDay);
        this.group = group;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
