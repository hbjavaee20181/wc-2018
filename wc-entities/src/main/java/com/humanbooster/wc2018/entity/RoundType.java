package com.humanbooster.wc2018.entity;

import java.util.stream.Stream;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public enum RoundType {
    ROUND_16("round_16"),
    ROUND_8("round_8"),
    ROUND_4("round_4"),
    ROUND_2_LOSER("round_2_loser"),
    ROUND_2("round_2");

    private String rep;

    RoundType(String rep) {
        this.rep = rep;
    }

    public String getRep() {
        return rep;
    }

    public static RoundType fromRep(String rep) {
        return Stream.of(values())
                .filter(r -> r.getRep().equals(rep))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Cannot parse RoundType : " + rep));
    }
}
