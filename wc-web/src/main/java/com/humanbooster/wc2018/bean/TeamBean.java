package com.humanbooster.wc2018.bean;

import com.humanbooster.wc2018.entity.Team;
import com.humanbooster.wc2018.service.TeamService;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import java.util.List;

/**
 * Created by Ben on 22/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@ManagedBean
public class TeamBean {

    @EJB
    private TeamService teamService;

    public DataModel<Team> getTeamDataModel() {
        List<Team> allTeams = teamService.getAllTeams();
        return new ListDataModel<>(allTeams);
    }
}
