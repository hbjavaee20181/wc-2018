package com.humanbooster.wc2018.bean;

import com.humanbooster.wc2018.entity.Stadium;
import com.humanbooster.wc2018.service.StadiumService;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import java.util.List;

/**
 * Created by Ben on 22/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@ManagedBean
public class StadiumBean {

    @EJB
    private StadiumService stadiumService;

    public DataModel<Stadium> getStadiumDataModel() {
        List<Stadium> allStadiums = stadiumService.getAllStadiums();
        return new ListDataModel<>(allStadiums);
    }
}
