package com.humanbooster.wc2018.bean;

import com.humanbooster.wc2018.service.DataRefreshService;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

/**
 * Created by Ben on 22/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@ManagedBean
public class RefreshBean {

    @EJB
    private DataRefreshService dataRefreshService;

    public void refreshData() {
        dataRefreshService.refreshData();
    }
}
