package com.humanbooster.wc2018.bean;

import com.humanbooster.wc2018.model.GroupWithTeamsItem;
import com.humanbooster.wc2018.service.GroupService;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import java.util.List;

/**
 * Created by Ben on 22/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@ManagedBean
public class GroupBean {

    @EJB
    private GroupService groupService;

    public List<GroupWithTeamsItem> getGroupWithTeams() {
        return groupService.getGroupsWithTeams();
    }
}
