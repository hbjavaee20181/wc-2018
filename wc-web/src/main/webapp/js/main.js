var wc = {};

$(function () {
    console.log("Hey !");
    $('#refreshButton').click(function (e) {
        e.preventDefault();
        swal({
            title: 'Update the database',
            text: "Are you sure ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, update it !',
            cancelButtonText: 'mmh... i\'m not sure :('
        }).then(function (result) {
            if (result.value) {
                refreshData();
                swal({
                    title: 'Refreshing ...',
                    onOpen: function () {
                        swal.showLoading()
                    }
                });
            }
        });
    });
});

wc.refreshComplete = function () {
    swal.close();
    swal(
        'Done !',
        'All data have been refresh :-)',
        'success'
    );
};