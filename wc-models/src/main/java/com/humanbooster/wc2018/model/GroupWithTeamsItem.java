package com.humanbooster.wc2018.model;

import java.util.List;

/**
 * Created by Ben on 21/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class GroupWithTeamsItem {

    private String groupName;
    private String groupIdentifier;
    private List<TeamItem> teamItems;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupIdentifier() {
        return groupIdentifier;
    }

    public void setGroupIdentifier(String groupIdentifier) {
        this.groupIdentifier = groupIdentifier;
    }

    public List<TeamItem> getTeamItems() {
        return teamItems;
    }

    public void setTeamItems(List<TeamItem> teamItems) {
        this.teamItems = teamItems;
    }
}
