package com.humanbooster.wc2018.service;

import com.humanbooster.wc2018.dao.StadiumDao;
import com.humanbooster.wc2018.entity.Stadium;
import com.humanbooster.wc2018consumer.model.StadiumItem;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Stateless
public class StadiumService {

    @EJB
    private StadiumDao stadiumDao;

    public Long createStadiumIfNotExists(StadiumItem item) {
        Stadium stadium = stadiumDao.find(item.getId());
        if (stadium == null) {
            System.out.println("Creating stadium " + item.getName() + " ...");
            stadium = new Stadium(
                    item.getId(),
                    item.getName(),
                    item.getCity(),
                    item.getLat(),
                    item.getLng(),
                    item.getImage());
            stadiumDao.create(stadium);
        }
        return stadium.getId();
    }

    public List<Stadium> getAllStadiums() {
        return stadiumDao.getAllStadiums();
    }
}
