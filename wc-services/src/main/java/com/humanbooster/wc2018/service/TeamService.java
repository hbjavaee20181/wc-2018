package com.humanbooster.wc2018.service;


import com.humanbooster.wc2018.dao.TeamDao;
import com.humanbooster.wc2018.entity.Team;
import com.humanbooster.wc2018consumer.model.TeamItem;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Stateless
public class TeamService {

    @EJB
    private TeamDao teamDao;

    public Long createTeamIfNotExists(TeamItem item) {
        Team team = teamDao.find(item.getId());
        if (team == null) {
            System.out.println("Creating team " + item.getName() + " ...");
            team = new Team(
                    item.getId(),
                    item.getName(),
                    item.getFifaCode(),
                    item.getIso2(),
                    item.getFlag(),
                    item.getEmoji());
            teamDao.create(team);
        }
        return team.getId();
    }

    public List<Team> getAllTeams() {
        return teamDao.getAllTeams();
    }

    public List<Team> getAllTeamsByGroup(Long groupId) {
        return teamDao.getAllTeamsByGroup(groupId);
    }
}
