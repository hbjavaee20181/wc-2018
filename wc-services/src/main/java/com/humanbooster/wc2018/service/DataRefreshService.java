package com.humanbooster.wc2018.service;


import com.humanbooster.wc2018consumer.model.GroupItem;
import com.humanbooster.wc2018consumer.model.KnockoutItem;
import com.humanbooster.wc2018consumer.model.WordCupJsonItem;
import com.humanbooster.wc2018consumer.service.WorldCupWebServiceConsumer;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Map;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Stateless
public class DataRefreshService {

    @EJB
    private StadiumService stadiumService;

    @EJB
    private TvChannelService tvChannelService;

    @EJB
    private TeamService teamService;

    @EJB
    private GroupService groupService;

    @EJB
    private MatchService matchService;

    public void refreshData() {
        WordCupJsonItem allData = WorldCupWebServiceConsumer.getInstance().getAllData();

        // Update Stadiums
        System.out.println("Updating Stadiums ...");
        allData.getStadiums().forEach(stadiumService::createStadiumIfNotExists);

        // Update Tv Channels
        System.out.println("Updating Tv Channels ...");
        allData.getTvchannels().forEach(tvChannelService::createTvChannelIfNotExists);

        // Update Teams
        System.out.println("Updating Teams ...");
        allData.getTeams().forEach(teamService::createTeamIfNotExists);

        // Update Groups and Group matches
        System.out.println("Updating Groups and group matches...");
        for (Map.Entry<String, GroupItem> entry : allData.getGroups().entrySet()) {
            String groupStringIdentifier = entry.getKey();
            GroupItem groupItem = entry.getValue();

            // Create the group
            Long groupId = groupService.createGroupIfNotExists(groupStringIdentifier, groupItem);

            // Update group matches for the current group
            entry.getValue().getMatches().forEach((m) -> matchService.createGroupMatchIfNotExists(groupId, m));
        }

        // Update knockout matches
        System.out.println("Updating Knockout matches...");
        for (Map.Entry<String, KnockoutItem> entry : allData.getKnockout().entrySet()) {
            String knockoutIdentifier = entry.getKey();
            entry.getValue().getMatches().forEach(v -> matchService.createFinalMatch(knockoutIdentifier, v));
        }
    }
}
