package com.humanbooster.wc2018.service;


import com.humanbooster.wc2018.dao.GroupDao;
import com.humanbooster.wc2018.dao.TeamDao;
import com.humanbooster.wc2018.entity.Group;
import com.humanbooster.wc2018.entity.Team;
import com.humanbooster.wc2018.model.GroupWithTeamsItem;
import com.humanbooster.wc2018.model.TeamItem;
import com.humanbooster.wc2018consumer.model.GroupItem;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Stateless

public class GroupService {

    @EJB
    private GroupDao groupDao;

    @EJB
    private TeamDao teamDao;

    public Long createGroupIfNotExists(String strId, GroupItem item) {
        Group group = groupDao.findByStringIdentifier(strId);
        if (group == null) {
            System.out.println("Creating group " + item.getName() + " ...");
            group = new Group(
                    strId,
                    item.getName());
            groupDao.create(group);
        }
        return group.getId();
    }

    public List<Group> getAllGroups() {
        return groupDao.getAllGroups();
    }

    public List<GroupWithTeamsItem> getGroupsWithTeams() {
        List<GroupWithTeamsItem> items = new ArrayList<>();
        List<Group> allGroups = groupDao.getAllGroups();
        for (Group g : allGroups) {
            GroupWithTeamsItem item = new GroupWithTeamsItem();
            item.setGroupName(g.getGroupName());
            item.setGroupIdentifier(g.getStrIdentifier());
            List<Team> allTeamsByGroup = teamDao.getAllTeamsByGroup(g.getId());
            List<TeamItem> teams = new ArrayList<>();
            for (Team t : allTeamsByGroup) {
                TeamItem ti = new TeamItem();
                ti.setName(t.getName());
                ti.setFlag(t.getFlag());
                teams.add(ti);
            }
            item.setTeamItems(teams);
            items.add(item);
        }
        return items;
    }
}