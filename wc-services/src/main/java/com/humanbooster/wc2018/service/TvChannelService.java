package com.humanbooster.wc2018.service;


import com.humanbooster.wc2018.dao.TvChannelDao;
import com.humanbooster.wc2018.entity.TvChannel;
import com.humanbooster.wc2018consumer.model.TvChannelItem;

import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Stateless
public class TvChannelService {

    @EJB
    private TvChannelDao tvChannelDao;

    public Long createTvChannelIfNotExists(TvChannelItem item) {
        TvChannel channel = tvChannelDao.find(item.getId());
        if (channel == null) {
            System.out.println("Creating Tv channel " + item.getName() + " ...");
            channel = new TvChannel(
                    item.getId(),
                    item.getName(),
                    item.getIcon(),
                    item.getCountry(),
                    item.getIso2(),
                    item.getLang());
            tvChannelDao.create(channel);
        }
        return channel.getId();
    }
}
