package com.humanbooster.wc2018.dao.impl;


import com.humanbooster.wc2018.dao.MatchDao;
import com.humanbooster.wc2018.entity.Match;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Stateless
public class JpaMatchDao extends JpaCrudDao<Match, Long> implements MatchDao {

    @Override
    public Match findByNameId(Long name) {
        TypedQuery<Match> q = em.createQuery("select m from Match AS m where m.name = :name", Match.class);
        q.setParameter("name", name);
        try {
            return q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    protected Class<Match> getTargetClass() {
        return Match.class;
    }
}
