package com.humanbooster.wc2018.dao.impl;


import com.humanbooster.wc2018.dao.StadiumDao;
import com.humanbooster.wc2018.entity.Stadium;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Stateless
public class JpaStadiumDao extends JpaCrudDao<Stadium, Long> implements StadiumDao {

    @Override
    protected Class<Stadium> getTargetClass() {
        return Stadium.class;
    }

    @Override
    public List<Stadium> getAllStadiums() {
        TypedQuery<Stadium> q = em.createQuery("select s from Stadium as s", Stadium.class);
        return q.getResultList();
    }
}
