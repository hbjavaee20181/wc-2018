package com.humanbooster.wc2018.dao;


import com.humanbooster.wc2018.entity.Stadium;

import java.util.List;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public interface StadiumDao extends CrudDao<Stadium, Long> {
    List<Stadium> getAllStadiums();
}
