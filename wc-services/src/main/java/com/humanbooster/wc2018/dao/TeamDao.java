package com.humanbooster.wc2018.dao;


import com.humanbooster.wc2018.entity.Team;

import java.util.List;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public interface TeamDao extends CrudDao<Team, Long> {
    List<Team> getAllTeams();
    List<Team> getAllTeamsByGroup(Long groupId);
}
