package com.humanbooster.wc2018.dao.impl;


import com.humanbooster.wc2018.dao.GroupDao;
import com.humanbooster.wc2018.entity.Group;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Stateless
public class JpaGroupDao extends JpaCrudDao<Group, Long> implements GroupDao {

    @Override
    protected Class<Group> getTargetClass() {
        return Group.class;
    }

    @Override
    public Group findByStringIdentifier(String strId) {
        TypedQuery<Group> q = em.createQuery("select g from Group AS g where g.strIdentifier = :strIdentifier", Group.class);
        q.setParameter("strIdentifier", strId);
        try {
            return q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List<Group> getAllGroups() {
        TypedQuery<Group> q = em.createQuery("select g from Group as g", Group.class);
        return q.getResultList();
    }
}
