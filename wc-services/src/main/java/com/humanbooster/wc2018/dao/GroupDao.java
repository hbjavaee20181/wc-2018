package com.humanbooster.wc2018.dao;


import com.humanbooster.wc2018.entity.Group;

import java.util.List;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public interface GroupDao extends CrudDao<Group, Long> {
    Group findByStringIdentifier(String strId);
    List<Group> getAllGroups();
}
