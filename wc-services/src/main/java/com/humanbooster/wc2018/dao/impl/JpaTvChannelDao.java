package com.humanbooster.wc2018.dao.impl;


import com.humanbooster.wc2018.dao.TvChannelDao;
import com.humanbooster.wc2018.entity.TvChannel;

import javax.ejb.Stateless;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Stateless
public class JpaTvChannelDao extends JpaCrudDao<TvChannel, Long> implements TvChannelDao {


    @Override
    protected Class<TvChannel> getTargetClass() {
        return TvChannel.class;
    }
}
