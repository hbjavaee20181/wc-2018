package com.humanbooster.wc2018.dao;


import com.humanbooster.wc2018.entity.FinalMatch;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public interface FinalMatchDao extends CrudDao<FinalMatch, Long> {

}
