package com.humanbooster.wc2018.dao.impl;

import com.humanbooster.wc2018.dao.GroupMatchDao;
import com.humanbooster.wc2018.entity.GroupMatch;

import javax.ejb.Stateless;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Stateless
public class JpaGroupMatchDao extends JpaCrudDao<GroupMatch, Long> implements GroupMatchDao {

    @Override
    protected Class<GroupMatch> getTargetClass() {
        return GroupMatch.class;
    }

}
