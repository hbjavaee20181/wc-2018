package com.humanbooster.wc2018.dao.impl;


import com.humanbooster.wc2018.dao.TeamDao;
import com.humanbooster.wc2018.entity.MatchType;
import com.humanbooster.wc2018.entity.Team;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Stateless
public class JpaTeamDao extends JpaCrudDao<Team, Long> implements TeamDao {

    @Override
    protected Class<Team> getTargetClass() {
        return Team.class;
    }

    @Override
    public List<Team> getAllTeams() {
        TypedQuery<Team> q = em.createQuery("select t from Team as t", Team.class);
        return q.getResultList();
    }

    @Override
    public List<Team> getAllTeamsByGroup(Long groupId) {
        TypedQuery<Team> q = em.createQuery("select distinct t from Team as t, GroupMatch gmh, GroupMatch gma join t.awayMatchs am join t.homeMatchs hm " +
                "where am.type = :aType and hm.type = :hType and gmh = hm " +
                "and gma = am and gmh.group.id = :ghId and gma.group.id = :gaId", Team.class);
        q.setParameter("aType", MatchType.GROUP);
        q.setParameter("hType", MatchType.GROUP);
        q.setParameter("ghId", groupId);
        q.setParameter("gaId", groupId);
        return q.getResultList();
    }
}
