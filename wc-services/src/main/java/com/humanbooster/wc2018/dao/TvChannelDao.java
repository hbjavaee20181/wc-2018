package com.humanbooster.wc2018.dao;


import com.humanbooster.wc2018.entity.TvChannel;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public interface TvChannelDao extends CrudDao<TvChannel, Long> {
}
