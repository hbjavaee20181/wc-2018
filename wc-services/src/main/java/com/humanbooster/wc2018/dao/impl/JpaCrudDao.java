package com.humanbooster.wc2018.dao.impl;


import com.humanbooster.wc2018.dao.CrudDao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.io.Serializable;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public abstract class JpaCrudDao<T, PK extends Serializable> implements CrudDao<T, PK> {

    @PersistenceContext
    protected EntityManager em;

    @Override
    public T create(T t) {
        em.persist(t);
        return t;
    }

    @Override
    public T find(PK id) {
        try {
            return em.find(getTargetClass(), id);
        } catch (NoResultException e) {
            System.out.println("No result found for id " + id + " and entity " + getTargetClass().getSimpleName());
            return null;
        }
    }

    @Override
    public T update(T t) {
        return em.merge(t);
    }

    @Override
    public void delete(T t) {
        em.remove(t);

    }

    protected abstract Class<T> getTargetClass();
}
