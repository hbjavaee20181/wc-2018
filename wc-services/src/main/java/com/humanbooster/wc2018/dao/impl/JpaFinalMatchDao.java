package com.humanbooster.wc2018.dao.impl;


import com.humanbooster.wc2018.dao.FinalMatchDao;
import com.humanbooster.wc2018.entity.FinalMatch;

import javax.ejb.Stateless;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Stateless
public class JpaFinalMatchDao extends JpaCrudDao<FinalMatch, Long> implements FinalMatchDao {

    @Override
    protected Class<FinalMatch> getTargetClass() {
        return FinalMatch.class;
    }
}
